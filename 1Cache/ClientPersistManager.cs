using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Core.Middleware.Interfaces;
using Core.Middleware.MetaData;
using Core.Middleware.Services;
using System.Threading;
using Inc.Logging;
using Logger = Inc.Logging.Logger;

namespace Core.Client
{
    /// <summary>
    /// ������� ����� ����������� ��������� �������� ��������
    /// </summary>
    public abstract class ClientPersistManager : IClientPersistManager
    {
        private BoolSetting _casheUsing = new BoolSetting("CasheUsing", true);
        private bool CasheUsing
        { get { return _casheUsing.Value; } }

        private readonly ClientCashe<Persist> _cashe = new ClientCashe<Persist>(); // ���������� ���

        private string _strManagerTypeForLog = "";
        private string StrManagerTypeForLog
        {
            get
            {
                if (_strManagerTypeForLog == "")
                {
                    //_strManagerTypeForLog = GetType().ToString();
                    //_strManagerTypeForLog = _strManagerTypeForLog.Substring(_strManagerTypeForLog.LastIndexOf('.') + 1) + ":Base.";
                    _strManagerTypeForLog = GetType() + ":Base.";
                }
                return _strManagerTypeForLog;
            }
        }

        #region IPersistManager Members

        public virtual void UnPack(PersistObjectData packedObjectData, Persist item)
        {
            PackerManager.UnPack(packedObjectData, item);
        }

        public Type GetItemType()
        {
            return ItemsType;
        }

        public PersistPackedObjectData Pack(Persist item)
        {
            return PackerManager.Pack(item);
        }

        public Persist UnPack(PersistPackedObjectData packedObjectData)
        {
            PersistObjectData po = new PersistObjectData(packedObjectData);
            Persist item = (Persist)Activator.CreateInstance(ItemsType, new object[] { po });
            return item;
        }

        /// <summary>
        /// ������������ ���������
        /// </summary>
        /// <param name="info">�ᒺ�� ����������� ����������</param>
        public void ConfigurateManager(IncInterfaceInfo info)
        { _cashe.SetObjectsCasheUpLimit(info.ObjectsCasheUpLimit); }

        /// <summary>
        /// ���������� �������� �ᒺ��� � ��
        /// </summary>
        /// <param name="id">������������� �ᒺ���</param>
        /// <returns>��������� �ᒺ��� � ��</returns>
        public bool IsItemExist(Guid id)
        {
            if (id == Guid.Empty)
                return false;
            bool res = false;
            if (CasheUsing)
                res = _cashe.GetItem(id) != null;
            if (!res)
                try { res = (bool)ServerInvokation("IsItemExist", new object[] { id }); }
                catch { Logger.WriteError(this, StrManagerTypeForLog + "IsItemExist(Guid id)", "������� �������� �������� �ᒺ��� � id = " + id + " �� ������"); }
            return res;
        }

        /// <summary>
        /// ��������� ��������� �������
        /// </summary>
        /// <param name="id">������������� �������</param>
        /// <returns>������� ������ (null, ���� �� ������)</returns>
        public Persist GetItem(Guid id)
        {
            if (id == Guid.Empty)
                return null;
            Persist item = null;
            //if ((bool)appSettingsReader.GetValue("CasheUsing", typeof(bool)))
            if (CasheUsing)
                item = _cashe.GetItem(id);
            if (item == null)
                try
                {
                    PersistPackedObjectData objData;
                    objData = (PersistPackedObjectData)ServerInvokation("GetItemData", new object[] { id });
                    if (objData != null)
                    {
                        item = UnPack(objData);
                        if (CasheUsing)
                            _cashe.AddItem(item);
                    }
                }
                catch { Logger.WriteError(this, StrManagerTypeForLog + "GetItem(Guid id)", "������� ��������� �ᒺ��� �� ������� � id = " + id + "..."); }
            return item;
        }

        /// <summary>
        /// ��������� ��������� �������
        /// </summary>
        /// <param name="persistStamp">������� �������� �������, ������� ����� ��������</param>
        /// <returns>������� ������ (null, ���� �� ������)</returns>
        public Persist GetItem(PersistStamp persistStamp)
        {
            if (persistStamp == null)
                return null;

            Persist persistItem = null;
            if (CasheUsing)
                persistItem = _cashe.GetItem(persistItem.ID);

            if (persistItem != null)
                return persistItem;

            try
            {
                PersistPackedObjectData objData;
                objData = (PersistPackedObjectData)ServerInvokation("GetItemDataExt", new object[] { persistStamp });
                if (objData != null)
                {
                    persistItem = UnPack(objData);
                    if (CasheUsing)
                        _cashe.AddItem(persistItem);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, "GetItem", "������� ��� ��������� ��'����", ex);
            }
            return persistItem;
        }

        /// <summary>
        /// ��������� ������ ���� �������� ��������
        /// </summary>
        /// <returns>����� ���� ��������� �������� (������, ���� �� ������ �� ���� ������)</returns>
        public Persist[] GetAllItems()
        {
            return GetAllItems(false);
        }


        /// <summary>
        /// ��������� ������ ���� �������� ��������
        /// </summary>
        /// <param name="IsOld">���������� �� ��������� �������</param>
        /// <returns>����� ���� ��������� �������� (������, ���� �� ������ �� ���� ������)</returns>
        public Persist[] GetAllItems(bool IsOld)
        {
            Logger.WriteInfo(this, StrManagerTypeForLog + "GetAllItems()", "�������� ��� ������� ���� " + ItemsType.Name + "...");
            try
            {
                Persist[] res = new Persist[0];
                Guid[] ids = null;
                ids = (Guid[])ServerInvokation("GetAllItemIDs", new object[] { IsOld });

                if (ids == null)
                    Logger.WriteError(this, StrManagerTypeForLog + "GetAllItems()", "�������� null  IDs !!!! �������� ���� " + ItemsType);
                else
                {
                    res = GetItems(ids);
                    Logger.WriteInfo(this, StrManagerTypeForLog + "GetAllItems()", "�������� " + ids.Length + " IDs �������� ���� " + ItemsType);
                }
                return res;
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, StrManagerTypeForLog + "GetAllItems()", "������� ������ �� ��������� �������", ex);
                return new Persist[0];
            }

        }

        /// <summary>
        /// ��������� ������ ������ ������ ��'����
        /// </summary>
        /// <param name="topCount">ʳ������ ������ �������� ���������</param>
        public Persist[] GetTopItems(int topCount)
        {
            Logger.WriteInfo(this, StrManagerTypeForLog + "GetTopItems()", "�������� ��� ������� ���� " + ItemsType.Name + "...");
            try
            {
                Guid[] ids = null;
                Persist[] res = new Persist[0];
                ids = (Guid[])ServerInvokation("GetTopItemsID", new object[] { topCount });

                if (ids == null)
                    Logger.WriteError(this, StrManagerTypeForLog + "GetTopItems()", "�������� null  IDs !!!! �������� ���� " + ItemsType);
                else
                {
                    Logger.WriteInfo(this, StrManagerTypeForLog + "GetTopItems()", "�������� " + ids.Length + " IDs �������� ���� " + ItemsType);
                    res = GetItems(ids);
                }
                return res;
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, StrManagerTypeForLog + "GetTopItems()", "������� ������ �� ��������� �������", ex);
                return new Persist[0];
            }
        }

        protected virtual Persist[] GetTypedArray(int Length)
        {
            return new Persist[Length];
        }

        /// <summary>
        /// ��������� ������ �������� ��������
        /// </summary>
        /// <param name="ids">����� ��������������� ��������</param>
        /// <returns>����� ��������� �������� (������, ���� �� ������ �� ���� ������)</returns>
        public Persist[] GetItems(Guid[] ids)
        {
            // �� �������� �������� � ������� �ᒺ���
            bool isNewDataCashing = CasheUsing;
            isNewDataCashing = isNewDataCashing && ids.Length < _cashe.ObjectsCasheUpLimit / 2;
            // ������ ID �ᒺ���, ���� � ���� �� ���������
            List<Guid> lstNotObjID = new List<Guid>();
            Persist[] res = GetTypedArray(ids.Length); // ������������ �����
            int index = 0; // �������� ��������� �������� ������������� ������

            foreach (Guid id in ids) // ����� �������� � ����
            {
                Persist item = null;
                if (CasheUsing)
                    item = _cashe.GetItem(id);
                if (item == null)
                    lstNotObjID.Add(id);
                else
                {
                    res[index] = item;
                    index++;
                }
            }

            int countElementTotal = ids.Length;
            int countElementFromCache = ids.Length - lstNotObjID.Count;
            int countElementNeadLoad = lstNotObjID.Count;
            int countElementLoadFromServer = 0;

            if (CasheUsing)
                Logger.WriteInfoDebug(GetType(), StrManagerTypeForLog + "GetItems(Guid[] ids)", countElementFromCache + " �ᒺ��� ���� �������� � ���� � �� " + lstNotObjID.Count + " �ᒺ��� ������� ������� � �������");
            else Logger.WriteWarning(this, StrManagerTypeForLog + "GetItems(Guid[] ids)", "�볺������� ��� �ᒺ��� ��������� � ���������������� ����. �� ���� �������� �� ����������� ������ �������!");

            if (lstNotObjID.Count > 0) // ������� ��������� �� ������� �� ������
            {
                Logger.WriteInfoDebug(GetType(), StrManagerTypeForLog + "GetItems(Guid[])", "�������� �������� ����� ����� ��� �ᒺ��� �� ������� IDs...");
                object tmp = ServerInvokation("GetItemsData", new object[] { lstNotObjID.ToArray() });
                PersistPackedObjectData[] objsData = (PersistPackedObjectData[])tmp;
                countElementLoadFromServer = objsData.Length;
                Logger.WriteInfoDebug(GetType(), StrManagerTypeForLog + "GetItems(Guid[])", "�������� " + objsData.Length + " ����� ����� ��� �ᒺ��� ���� " + ItemsType + ". �������� �������� Persist-�ᒺ��� � ����� �����...");

                foreach (PersistPackedObjectData objData in objsData)
                {
                    Persist item;
                    item = UnPack(objData);
                    if (isNewDataCashing)
                    {
                        if (!_cashe.ObjectsCashe.ContainsKey(item.ID))
                            _cashe.AddItem(item);
                        else
                            _cashe.ObjectsCashe[item.ID] = item;
                    }
                    res[index] = item;
                    index++;
                }
                Logger.WriteInfoDebug(GetType(), StrManagerTypeForLog + "GetItems(Guid[])", "Persist-oᒺ��� ���������");
            }
            if (countElementNeadLoad == countElementLoadFromServer)
                Logger.WriteInfo(this, StrManagerTypeForLog + "GetItems(Guid[] ids)", "����������� " + countElementTotal + " �ᒺ���. � ��� � ���� - " + countElementFromCache);
            else
                Logger.WriteWarning(this, StrManagerTypeForLog + "GetItems(Guid[] ids)", "����������� " + countElementTotal + " �ᒺ���. � ��� � ���� - " + countElementFromCache + "; �� ����������� � ������� �������� - " + (countElementNeadLoad - countElementLoadFromServer));
            Logger.WriteInfo(this, "Base.GetItems", "����������� " + countElementTotal + " �ᒺ���", "����������� " + countElementTotal + " �ᒺ���. " + Environment.NewLine + "� ��� � ���� - " + countElementFromCache);
            return res;
        }

        ///// <summary>
        ///// ��������� ������ �������� �������� �� �������
        ///// </summary>
        ///// <param name="query">������� �� ������� ��������</param>
        ///// <returns>����� ��������� �������� (������, ���� �� ������ �� ���� ������)</returns>
        //public virtual IPersist[] GetItems(string query)
        //{
        //   Logger.Instance.WriteInfo(StrManagerTypeForLog + "GetItems(query)", "�������� ������� ���� " + ItemsType.Name + " �� �������: " + query);
        //   //���� ������ ������ �����������, �� ��������� �� ���������� �ᒺ��� ����� �������
        //   if (ClientSettings.Instance.CasheUsing && _cashe.ContainsFunction("GetItems(" + query + ")"))
        //   {
        //      Logger.Instance.WriteInfo(StrManagerTypeForLog + "GetItems(query)", "����� ����������. ������ � ����");
        //      return _cashe.GetQueryResult("GetItems(" + query + ")");
        //   }
        //   //�������� ������ �������������� �ᒺ��� � �������, �� ������������� ���� �������
        //   Guid[] ids = null;
        //   Logger.Instance.WriteInfo(StrManagerTypeForLog + "GetItems(query)", "�������� �������� �� IDs �������� �ᒺ���...");
        //   Logger.Instance.WriteInfo(StrManagerTypeForLog + "GetItems(query)", "query = " + query);
        //   try
        //   {
        //      ids = (Guid[])ServerInvokation("GetItemIDs", new object[] { query });
        //      Logger.Instance.WriteInfo(StrManagerTypeForLog + "GetItems(query)", "�������� " + ids.Length + " IDs �� ������� '" + query + "'");
        //   }
        //   catch (Exception) { Logger.Instance.WriteError(StrManagerTypeForLog + "GetItems(query)", "�������� null  IDs !!!! ��������"); }
        //   if (ids != null)
        //   {
        //      IPersist[] ips = GetItems(ids);
        //      if (ClientSettings.Instance.CasheUsing)
        //         _cashe.AddFunction("GetItems(" + query + ")", ips);
        //      return ips;
        //   }
        //   return new IPersist[] { };
        //}

        /// <summary>
        /// ���������� ������� � �� (��� ������� ���������� ����������)
        /// </summary>
        /// <param name="item">������, ������� ���� ���������</param>
        public virtual void SaveItem(Persist item)
        {
            try
            {
                PersistPackedObjectData po = Pack(item);
                if (item.IsNew)
                    _cashe.AddItem(item);
                po.ObjectData.Add("ISNEW", item.IsNew);
                ServerInvokation("SaveItem", new object[] { po });
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, StrManagerTypeForLog + "SaveItem()", "������ ���������� �������", ex);
                throw new Exception(StrManagerTypeForLog + "SaveItem(): ������ ���������� �������", ex);
            }
        }

        /// <summary>
        /// ���������� �������� � ��
        /// (��� ������� ���������� ����������)
        /// </summary>
        /// <param name="items">������, ������� ���� ���������</param>
        [Obsolete("����������� ������� SaveItems() �� ����������� ���� Persist: ConcretePersistType.SaveItems(itemsForSave)")]
        public virtual void SaveItems(Persist[] items)
        {
            try
            {
                var ppods = new List<PersistPackedObjectData>();
                foreach (var item in items)
                {
                    var ppod = Pack(item);
                    if (item.IsNew)
                        _cashe.AddItem(item);
                    ppod.ObjectData.Add("ISNEW", item.IsNew);
                    ppods.Add(ppod);
                }
                ServerInvokation("SaveItems", new object[] { ppods.ToArray() });
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, StrManagerTypeForLog + "SaveItems()", "������� ���������� �ᒺ���", ex);
                throw new Exception(StrManagerTypeForLog + "SaveItems(): ������� ���������� �ᒺ���", ex);
            }
        }

        public void BackUpItems(string fname, Persist[] items)
        {
            try
            {
                var ppods = new List<PersistPackedObjectData>();
                foreach (var item in items)
                {
                    //if (item.IsNew) continue;
                    var ppod = Pack(item);
                    ppod.ObjectData.Add("ISNEW", item.IsNew);
                    ppods.Add(ppod);
                }
                SBSettings<List<PersistPackedObjectData>>.SaveImportObject(fname, ppods);
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, StrManagerTypeForLog + "BackUpItems()", "������� �������� �ᒺ���", ex);
                throw new Exception(StrManagerTypeForLog + "BackUpItems(): ������� �������� �ᒺ���", ex);
            }
        }

        /// <summary>
        /// ������� ��������� �������� ����
        /// </summary>
        /// <returns></returns>
        public DateTime GetServerDate()
        {
            try
            {
                return (DateTime)ServerInvokation("GetServerDate", new object[] { });
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, "GetServerDate()", "������� ��������� �������� ���� �� ����", ex);
            }
            return new DateTime();
        }

        /// <summary>
        /// ��������� �� ����������� ������������ ��'���
        /// </summary>
        /// <param name="data">���������� ��'���</param>
        public void ImportItems(PersistPackedObjectData[] data)
        {
            try
            {
                ServerInvokation("SaveItems", new object[] { data });
            }
            catch (Exception ex)
            { Logger.WriteError(this, "ImportItems()", "������� ������� �ᒺ���", ex); }
        }

        /// <summary>
        /// �������� ������� �� �� (��� ������� ���������� ����������)
        /// </summary>
        /// <param name="item">������, ������� ���� �������</param>
        /// <param name="isDelFromDB">true- �������� � ��, false- ��������� IsOld = 1</param>
        public virtual void DeleteItem(Persist item, bool isDelFromDB)
        {
            if (!item.IsNew)
            {
                if (CasheUsing)
                    DeletItemFromCache(item.ID);
                ServerInvokation("DeleteItem", new object[] { item.ID, isDelFromDB });
            }
        }

        /// <summary>
        /// �������� ������� �� �� (��� ������� ���������� ����������)
        /// </summary>
        /// <param name="item">������, ������� ���� �������</param>
        public virtual void DeleteItem(Persist item)
        {
            DeleteItem(item, false);
        }
        #endregion

        private IncRemoteCall CreateMessage(string strMetodName, object[] parameters, string dataTypeFullName)
        {

            IncRemoteCall message = new IncRemoteCall();
            Guid messageID = Guid.NewGuid();

            message.Parametrs.Add(IncRemoteCallParamType.MethodParameters, parameters);
            message.Parametrs.Add(IncRemoteCallParamType.TypeName, dataTypeFullName);
            message.Parametrs.Add(IncRemoteCallParamType.MethodName, strMetodName);
            message.Parametrs.Add(IncRemoteCallParamType.IsConteintInterfacingType, true);
            message.Parametrs.Add(IncRemoteCallParamType.MessageIdentifier, messageID);

            message.NameFunction = strMetodName;
            message.TypeObject = dataTypeFullName;

            return message;
        }

        protected object ServerInvokation(string strMetodName, object[] parameters)
        {
            CheckPoint point = Logger.CheckPointStart(LogLevel.Important, "{0}. ��������� �������� ������� '{1}'", StrManagerTypeForLog, strMetodName);
            DateTime dateStart = DateTime.Now;

            string dataTypeFullName = ObjMngFactory.GetDataTypeFullNameByManagerFullName(GetType().FullName);
            object[] paramsForMess = GetParamsForMessage(parameters);

            IncRemoteCall mes = CreateMessage(strMetodName, paramsForMess, dataTypeFullName);
            IncRemoteCall mesRet;
            try
            {
                mesRet = InvokeMessage(mes);
            }
            catch (Exception ex)
            {
                Logger.WriteError(this, "ClientPersistManager.ServerInvokation()", StrManagerTypeForLog + " ������� ��������� ������� " + dataTypeFullName + "." + strMetodName + " �� ������. ", ex);
                Console.WriteLine(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                Console.WriteLine(" ClientPersistManager.ServerInvokation() ������� ��������� ������� " + dataTypeFullName + "." + strMetodName + " �� ������. " + ex.Message);
                Console.WriteLine(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
                point.Close();
                throw;
            }
            var ret = mesRet.Parametrs[IncRemoteCallParamType.ResultMethod];

            EmulateBadNetwork();

            DateTime dateEnd = DateTime.Now;
            CollectInvokeMessageStatistic(dateStart, dateEnd, strMetodName, dataTypeFullName, mesRet);
            point.Close();
            return ret;
        }

        private void CollectInvokeMessageStatistic(DateTime dateStart, DateTime dateEnd, string strMetodName, string dataTypeFullName, IncRemoteCall mesRet)
        {
            TimeSpan time = dateEnd.Subtract(dateStart);

            TimeSpan timeInServer = new TimeSpan(0);
            object servTime = mesRet.Parametrs[IncRemoteCallParamType.TimeExecuting];
            if (servTime != null)
                timeInServer = (TimeSpan)servTime;

            Logger.WriteWarning(this, StrManagerTypeForLog + "ServerInvokation()", "�������� ����� " + dataTypeFullName + "." + strMetodName + "() �� " + time.TotalSeconds + " �. (" + timeInServer.TotalSeconds + " - �� ������)");
        }

        private static void EmulateBadNetwork()
        {
            if (ClientSettings.IsEmulationBadNetwork && ClientSettings.EmulationBadNetworkMillisecondDelay > 0)
                Thread.Sleep(ClientSettings.EmulationBadNetworkMillisecondDelay);
        }

        internal static IncRemoteCall InvokeMessage(IncRemoteCall mes)
        {
            IncRemoteCall mesRet = null;
            bool executed = false;
            bool restoredConnectionFailed = true;

            while (!executed)
            {
                try
                {
                    //ISession currentSession = Passport.CurrentUserSession;
                    Session currentSession = Session.UserSession;
                    if (currentSession == null)
                        throw new Exception("������� ��������� �������� ������� - ������� ����");
                    mesRet = currentSession.InvokeMessage(mes);
                    executed = true;
                    restoredConnectionFailed = false;
                }
                catch (System.ServiceModel.CommunicationException)
                {
                    restoredConnectionFailed = !ClientInstance.Instance.RestoreConnection();
                }
                catch (System.Runtime.Remoting.RemotingException)
                {
                    restoredConnectionFailed = !ClientInstance.Instance.RestoreConnection();
                }
                catch (System.Net.Sockets.SocketException)
                {
                    restoredConnectionFailed = !ClientInstance.Instance.RestoreConnection();
                }
                catch (Exception ex)
                {
                    Logger.WriteError(typeof(ClientPersistManager), "InvokeMessage()", " ������� ��������� ������� ", ex);
                    throw;
                }

                if (restoredConnectionFailed)
                    return null;
            }
            return mesRet;
        }

        private static object[] GetParamsForMessage(object[] parameters)
        {
            if (parameters == null)
                return new object[0];

            ArrayList lstPars = new ArrayList();
            foreach (object o in parameters)
            {
                Persist p = o as Persist;
                if (p != null)
                    lstPars.Add(new PersistObjectInfo(p));
                else
                {
                    Persist[] pp = o as Persist[];
                    if (pp == null)
                        lstPars.Add(o);
                    else
                    {
                        Logger.WriteInfo(typeof(ClientPersistManager), "~:Base.GetParamsForMessage()", "���������� ������ �������� ��������");
                        lstPars.Add(new PersistObjectInfoMass(pp));
                    }
                }
            }
            return lstPars.ToArray();
        }

        internal void DeletItemFromCache(Guid id)
        {
            _cashe.RemoveItem(id);
            Logger.WriteInfo(this, "ClientCashe.DeletItemFromCache", "��������� ��'���� � ���� ��� ���� " + ItemsType + " � ��������������� " + id);
        }

        /// <summary>
        /// ��������� ��������� ������� �� ���������� ������
        /// </summary>
        /// <param name="objData">���������� ��������� �������</param>
        /// <returns>�������� ������</returns>
        //public abstract Persist UnPack(PersistObjectData objData);

        /// <summary>
        /// ��������� ������������ ������������� ��������� �������
        /// </summary>
        /// <param name="obj">�������� ������</param>
        /// <returns>������������� ������������ ��������� �������</returns>
        //public virtual PersistObjectData Pack(Persist obj)
        //{
        //   return obj.Pack();
        //}

        /// <summary>
        /// ��� �������� � �������� �������� ��������
        /// </summary>
        public Type ItemsType
        {
            get
            {
                if (GetItemsType() == null)
                    throw new Exception(StrManagerTypeForLog + " �� ������������� ��� �������� � �������� �������� �������� � ���� '_itemsType'");
                return GetItemsType();
            }
        }

        /// <summary>
        /// ����� ��������� ���� �ᒺ��� (������������� � ������� ���������� ��������)
        /// </summary>
        protected virtual Type GetItemsType()
        {
            return null;
        }


        public Persist ReloadItem(Persist persistItem)
        {
            if (persistItem == null)
                return null;

            RemoveFromCasheItem(persistItem);

            persistItem.RefreshInnerPersist();
            return GetItem(persistItem.ID);
        }

        public Persist[] ReloadItems(Persist[] persistItems)
        {
            if (persistItems == null || persistItems.Length == 0)
                return new Persist[0];

            RemoveFromCasheItems(persistItems);

            List<Guid> lstPersistID = new List<Guid>();
            foreach (Persist item in persistItems)
            {
                item.RefreshInnerPersist();
                lstPersistID.Add(item.ID);
            }

            return GetItems(lstPersistID.ToArray());
        }

        public void RemoveFromCasheItem(Persist persistItem)
        {
            if (persistItem == null)
                return;

            _cashe.RemoveItem(persistItem);
        }

        public void RemoveFromCasheItems(Persist[] persistItems)
        {
            if (persistItems == null || persistItems.Length == 0)
                return;

            foreach (Persist item in persistItems)
                _cashe.RemoveItem(item);
        }

        public void RemoveFromCasheItemsByID(Guid[] persistItemsID)
        {
            if (persistItemsID == null || persistItemsID.Length == 0)
                return;

            foreach (Guid id in persistItemsID)
                _cashe.RemoveItem(id);
        }
    }
}
using System;
using System.Collections.Generic;
using Core.Middleware.Data;
using Core.Middleware.Interfaces;
using Inc.Logging;
using SBMessage = Core.Middleware.Data.SBMessage;

namespace Core.Client
{
    public class ClientCashe<TPersistItem> : ICashe where TPersistItem : Persist
    {
        private readonly Dictionary<Guid, TPersistItem> _objectsCashe = new Dictionary<Guid, TPersistItem>();
        private int _objectsCasheUpLimit = 1000;

        //private readonly Dictionary<string, Dictionary<Guid, TPersistItem>> _functionCashe = new Dictionary<string, Dictionary<Guid, TPersistItem>>();

        public Dictionary<Guid, TPersistItem> ObjectsCashe  {  get { return _objectsCashe; } }

        public int ObjectsCasheUpLimit {  get { return _objectsCasheUpLimit; } }

        internal void SetObjectsCasheUpLimit(int upLimit) { _objectsCasheUpLimit = upLimit;  }

        /// <summary>
        /// �������� �ᒺ��� � ���
        /// </summary>
        /// <param name="item">�ᒺ��, �� ������������</param>
        public void AddItem(TPersistItem item)
        {
            try
            {
                _objectsCashe.Add(item.ID, item);
                RegistrateItemInCashe(item.ID);
            }
            catch (ArgumentException) { Logger.WriteWarning(this, "AddItem()", "³������� ������ ���������� � ��� ��������� �ᒺ��� ����" + item.GetType() + " � ID " + item.ID); }
        }

        /// <summary>
        /// ��������� �ᒺ��� � ����
        /// </summary>
        /// <param name="item">�ᒺ��, �� ����������</param>
        public void RemoveItem(TPersistItem item) { RemoveItem(item.ID);  }

        /// <summary>
        /// ��������� �ᒺ��� � ����
        /// </summary>
        /// <param name="itemId">������������� �ᒺ���, �� ����������</param>
        public void RemoveItem(Guid itemId)
        {
            try
            {
                _objectsCashe.Remove(itemId);
                RemoveItemFromCashe(itemId);
            }
            catch (ArgumentException) { Logger.WriteWarning(this, "RemoveItem()", "³������� ������ ��������� � ���� ���������� �ᒺ���"); }
        }

        /// <summary>
        /// ��������� �ᒺ��� � ����
        /// </summary>
        /// <param name="id">������������� �ᒺ���</param>
        /// <returns>�ᒺ��</returns>
        public TPersistItem GetItem(Guid id) { return FindItem(id) ? _objectsCashe[id] : null; }

        #region ��������� ��������� Clock with Adaptive Replacement (CAR)
        // �������� �������� ������� � http://www.usenix.org/events/fast03/tech/full_papers/megiddo/megiddo.pdf
        // � http://www.usenix.org/event/usenix05/tech/general/full_papers/gill/gill.pdf
        // �� �������� ��������� ���������� �� ���������� ������� 
        private readonly List<ClockMember> _t1 = new List<ClockMember>(); // ��������� �� ��� �������� ����
        private readonly List<ClockMember> _t2 = new List<ClockMember>(); // ��������� �� ������������ �������� ����
        private readonly List<Guid> _b1 = new List<Guid>(); // �������������� ��������� ����� ��������
        private readonly List<Guid> _b2 = new List<Guid>(); // �������������� ��������� ������������� ��������
        private int _balansPosition; // ���� ������������ �� ������ � �������������� ���������� ����

        // ����� �������� � ����
        private bool FindItem(Guid id)
        {
            // ������������ ������������� ��� ������ �����
            SetGuidToFind(id);
            ClockMember newEl = _t1.Find(WithGuid);
            if (newEl == null) newEl = _t2.Find(WithGuid);
            if (newEl != null) // ���� ������� �������� ��� � �1 ��� � �2, �� ������� ���� ���� � ��������� ��������� �������
                return newEl.Bit = true;
            // ���� ������� �� �������� - ������ ����...
            return false;
        }
        // ���������� �������� � ����
        ////private bool _isSubscibe;
        internal void RegistrateItemInCashe(Guid id)
        {
             // TODO �����������������
             //if (Listener.ClientListener != null)
               //if (!_isSubscibe)
                //{
                //    Listener.ClientListener.MessageComeEvent += ClearChacheDirectiveFromServer;
                //    _isSubscibe = true;
                //}

            // ���������, �� ��� ������ �����
            bool historyMiss = !(_b1.Contains(id) || _b2.Contains(id));
            if (_t1.Count + _t2.Count == ObjectsCasheUpLimit)
            {
                // ��� ������ - �������� ������� � �����s
                DropItemFromCashe();
                // ���� �������, �� �������� ������� � �����
                if (historyMiss && (_t1.Count + _b1.Count == ObjectsCasheUpLimit))
                    _b1.RemoveAt(0);
                else if (historyMiss && (_t1.Count + _t2.Count + _b1.Count + _b2.Count == 2 * ObjectsCasheUpLimit))
                    _b2.RemoveAt(0);
            }
            if (historyMiss) // ��� ������� � �����
                _t1.Add(new ClockMember(id, false));
            else // ��������� � ������ ����
            {
                if (_b1.Contains(id)) // ��������� � ������ ���� B1
                    _balansPosition = Math.Min(_balansPosition + Math.Max(1, _b2.Count / _b1.Count), ObjectsCasheUpLimit);
                else // ��������� � ������ ���� B2
                    _balansPosition = Math.Max(_balansPosition - Math.Max(1, _b1.Count / _b2.Count), 0);
                _t2.Add(new ClockMember(id, false));
            } // �������� �� ���� � ����!!! � ������� �� ���� ������, � � ��� �� ��. ���������� ������!!!
        }

        private void ClearChacheDirectiveFromServer(MesAcount from, MesAcount to, SBMessage message)
        {
            if (message.Type != SBMessageType.ClearCache)
                return;
            Logger.WriteInfo(this, "ClearChacheDirectiveFromServer", "������� �������� ��'��� � �볺�������� ����");
            object id;
            object typeName;
            message.MessageData.TryGetValue(SBObjectType.Target, out typeName);
            message.MessageData.TryGetValue(SBObjectType.ClearCacheDirective, out id);
            if (id == null || typeName == null)
                return;
            try
            {
                var manager = (ClientPersistManager)ObjMngFactory.GetPersistManagerByDataTypeFullName((string)typeName);
                manager.DeletItemFromCache((Guid)id);
            } catch (Exception) {}
        }

        // ��������� ������ � �������� ����
        private void DropItemFromCashe()
        {
            var found = false; // ������� ���������� ���������
            do
            {
                if (_t1.Count >= Math.Max(1, _balansPosition)) // ������ �������� � �1
                    if (!_t1[0].Bit) // Bit = false -> ��������� ���������
                    {
                        found = true;
                        _b1.Add(_t1[0].Id); _t1.RemoveAt(0); // ����������� �������� � ������ �1 � ���� �1
                        SynchronizeWithServer(_t1[0].Id);//��������� ������� ��������� ��� ������ ������ �� ���� ������
                    }
                    else
                    {
                        _t1[0].Bit = false; // ������� �������� ������� �1 �� ��������� �� ��������� 
                        _t2.Add(_t1[0]); _t1.RemoveAt(0); // � ���������� ���� � ���� �2!!! ����� � ������ ������������� ��������
                    }
                else // ������ �������� � �2
                {
                    if (!_t2[0].Bit) // Bit = false -> ��������� ���������
                    {
                        found = true;
                        _b2.Add(_t2[0].Id); _t2.RemoveAt(0); // ����������� �������� � ������ �2 � ���� �2
                        SynchronizeWithServer(_t2[0].Id); //��������� ������� ��������� ��� ������ ������ �� ���� ������
                    }
                    else
                    {
                        _t2[0].Bit = false; // ������� �������� ������� �2 �� ��������� �� ��������� 
                        _t2.Add(_t2[0]); _t2.RemoveAt(0); // � ���������� ���� � ���� �2!!! ����� � ������ ������������� ��������
                    }
                }
            } while (!found);
        }
        #endregion

        /// <summary>
        /// �������� ��������� ������� �� �������� ������� � ���� (������������� � ObjectTracker �� �������)
        /// </summary>
        /// <param name="id">������������� �������, ������� �������</param>
        private static void SynchronizeWithServer(Guid id)
        {
            //TODO ��� ��������� ��������� ������������� ���� ��������� ���������� Listener.ClientListener.ClientRegularMessage, �� �������� ���� ����������
            //if (Listener.ClientListener == null)
            //   return;

            //Guid idUser = Passport.CurrentUserCredential.UserID;
            //SBMessage message;
            //if (!Listener.ClientListener.ClientRegularMessage.ContainsKey(idUser))
            //{
            //   message = new SBMessage(SBMessageType.ClienCacheSynhronize, new Dictionary<SBObjectType, object> { { SBObjectType.ClearCacheDirective, new object[] { id, idUser} } });
            //   Listener.ClientListener.ClientRegularMessage.Add(idUser, message);
            //}
            //else
            //{
            //   Listener.ClientListener.ClientRegularMessage.TryGetValue(idUser, out message);
            //   if (message == null)
            //      message = new SBMessage(SBMessageType.ClienCacheSynhronize, new Dictionary<SBObjectType, object>());
            //   message.MessageData.Add(SBObjectType.ClearCacheDirective, new object[] {id, idUser});
            //   Listener.ClientListener.ClientRegularMessage.Add(idUser, message);
            //}
        }

        // ��������� ����������� �������� ����
        private void RemoveItemFromCashe(Guid itemId)
        {
            // ������������ ������������� ��� ������ �����
            SetGuidToFind(itemId);
            ClockMember rem = _t1.Find(WithGuid);
            if (rem != null)
            { // ��������� � �1
                if (rem.Bit)
                    _b2.Add(rem.Id);
                else
                    _b1.Add(rem.Id);
                _t1.Remove(rem);
            }
            else
            {
                rem = _t2.Find(WithGuid);
                if (rem != null)
                { // ��������� � �2
                    _b2.Add(rem.Id);
                    _t2.Remove(rem);
                }
            }
        }

        private static Guid _guidToFind;
        private static void SetGuidToFind(Guid id) { _guidToFind = id; }
        private static bool WithGuid(ClockMember cm) { return cm.Id == _guidToFind; }

        public class ClockMember : IEquatable<ClockMember>
        {
            private readonly Guid _id; 
            public bool Bit { get; set; }

            public Guid Id 
            { 
                get { return _id; } 
            }

            public ClockMember(Guid id, bool bit) 
            { 
                _id = id; 
                Bit = bit; 
            }

            public bool Equals(ClockMember other) 
            { 
                return _id == other._id; 
            }
        }
    }
}

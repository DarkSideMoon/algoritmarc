# README #

*Adaptive replacement cache* 

All docs inside 

### Language implementation ###
C#

*### Algorithms ###*

### Links ###
[Wiki](https://en.wikipedia.org/wiki/Adaptive_replacement_cache)

[Article1](http://www.cs.cmu.edu/~15-440/READINGS/megiddo-computer2004.pdf)

[Article2](https://www.usenix.org/legacy/event/usenix05/tech/general/full_papers/gill/gill.pdf)
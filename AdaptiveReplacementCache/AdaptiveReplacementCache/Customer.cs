﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace AdaptiveReplacementCache
{
    public class Customer
    {

        private string _connString = "Server=localhost;Port=5432; User Id=postgres;Password=shark005;Database=postgres;";
        //This will be my meta-data that i can find a person by ID
        public Guid IdCustomer { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int Age { get; set; }
        


        public Customer() { }
        public Customer(string name, string city, int age)
        {
            Name = name;
            City = city;
            Age = age;
            IdCustomer = Guid.NewGuid();
        }

        public Customer GetCustomer()
        {
            Array values = Enum.GetValues(typeof(Customers));
            Random rnd = new Random();
            Customers _cust = (Customers)values.GetValue(rnd.Next(values.Length));

            Customer _customer = new Customer();

            string query = "select * from \"Customer\" where \"Name\" = '" + _cust + "'";
            NpgsqlConnection _conn = new NpgsqlConnection(_connString);
            NpgsqlCommand _cmd = new NpgsqlCommand(query, _conn);


            _conn.Open();
            NpgsqlDataReader reader = _cmd.ExecuteReader();

            while (reader.Read())
            {
                _customer.IdCustomer = (Guid)reader[1];
                _customer.Name = (string)reader[0];
                _customer.Age = (int)reader[2];
                _customer.City = (string)reader[3];
            }

            _conn.Close();

            return _customer;
        }

        public static Customer GetCustomerById(Guid _id)
        {
            string _connString = "Server=localhost;Port=5432; User Id=postgres;Password=shark005;Database=postgres;";
            Customer _customer = new Customer();

            string query = "select * from \"Customer\" where \"Id\" = '" + _id + "'";
            NpgsqlConnection _conn = new NpgsqlConnection(_connString);
            NpgsqlCommand _cmd = new NpgsqlCommand(query, _conn);


            _conn.Open();
            NpgsqlDataReader reader = _cmd.ExecuteReader();

            while (reader.Read())
            {
                _customer.IdCustomer = (Guid)reader[1];
                _customer.Name = (string)reader[0];
                _customer.Age = (int)reader[2];
                _customer.City = (string)reader[3];
            }

            _conn.Close();

            return _customer;
        }

        public override string ToString()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            return "\n\n[Id: " + IdCustomer + "\nName: " + Name + " City: " + City + " Age: " + Age + "]\n";
        }
    }

    enum Customers
    {
        Pasha,
        Diana, 
        Vlad, 
        Danya,
        Sasha
    }
}

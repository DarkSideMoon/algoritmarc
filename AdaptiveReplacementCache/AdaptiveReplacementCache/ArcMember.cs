﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaptiveReplacementCache
{
    public enum TypeMember
    {
        L1,
        L2
    }
    public class ArcMember
    {
        private readonly Guid _Id;
        private Customer _customer;
        public int Count { get; set; }
        public TypeMember TypeMember { get; set; }

        public Guid GuId
        {
            get { return _Id; }
        }

        public Customer Customer
        {
            get { return _customer; }
        }

        public ArcMember(Guid id, Customer customer)
        {
            _Id = id;
            _customer = customer;
        }

        public override string ToString()
        {
            return string.Format("\nId is: {0}\nCustomer in cache \nName: {1}\nCity: {2}\nAge: {3}\n",
                                         _Id, _customer.Name, _customer.City, _customer.Age);
        }

        public bool Equals(ArcMember member)//, Customer c)
        {
            return _Id == member._Id; //&& _customer == c;
        }

    }
}

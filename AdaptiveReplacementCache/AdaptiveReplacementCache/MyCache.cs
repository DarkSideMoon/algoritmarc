﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdaptiveReplacementCache
{
    public class MyCache<T> where T : class 
    {
        #region Властивості
        public int CurrentSizeL1 { get { return L1.Count; } }
        public int CurrentSizeL2 { get { return L2.Count; } }
        public int MaxcacheSize { get { return MaxSize; } }
        public Customer CurrentCustomer { get { return _customerCurrent; } }
        public int ItemsInObjectChach { get { return _objectsCache.Count; } }
        #endregion


        #region Інкапсуляція реалізації зберігання кешу
        private Customer _customerCurrent;
        
        private readonly Dictionary<Guid, Customer> _objectsCache = new Dictionary<Guid, Customer>();
        
        private const int MaxSize = MaxSizeL1 + MaxSizeL2 + 3; //L1 = 4 L2 = 3. If more than lets go to b1 and b1 ghosts
        private const int MaxSizeL1 = 4;
        private const int MaxSizeL2 = 3;
        
        private List<ArcMember> L1 = new List<ArcMember>(); //В цьому списку будуть зберігатись клієнти які НЕДАВНО потребували запиту до БД на свою персональну інформацію
        private List<ArcMember> L2 = new List<ArcMember>(); //В цьому списку будуть зберігатись клієнти які ЧАСТО зверталися до БД за своєю інформацією 
        
        private List<Guid> Ghost1 = new List<Guid>(); //Список привид який включає в себе ПОСИЛАННЯ на клієнта, а НЕ САМОГО клієнта, який був НЕДАВНО виключений з L1 списку. 
        private List<Guid> Ghost2 = new List<Guid>(); //Список привид тільки для списку L2 

        private static Guid _guidToFind;
        private static void SetToFind(Guid id) { _guidToFind = id; }
        private static bool GuidItem(ArcMember member) { return member.GuId == _guidToFind; }

        private int balance;
        #endregion


        #region Методи
        public void AddItem(Customer cust) 
        {
            try
            {
                //_objectsCache.Add(cust.IdCustomer, cust);
                AddItemInCache(cust);

            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Такий обєкт вже iснує у кешi. \nУдалiть цей обєкт з кешу i можна його дадати знову!");
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }
        }

        public void RemoveItem(Guid id) { RemoveItemFromCache(id); }

        public Customer GetItem(Guid id)
        {
            FindItem(id, out _customerCurrent);
            return _customerCurrent;
        }

        public void ShowInformation()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("--------------------------Information!--------------------------");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Кiлькiсть обєктiв у кешу: " + ItemsInObjectChach);
            Console.WriteLine("Останнiй записаний елемент в кеш: " + _customerCurrent.ToString());
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Кiлькiсть обєктiв у List1 (recent entries cache): " + L1.Count);
            Console.WriteLine("Кiлькiсть обєктiв у List2 (frequent entries cache): " + L2.Count);
            Console.WriteLine("Кiлькiсть обєктiв у Ghost1 (recent entries cache): " + Ghost1.Count);
            Console.WriteLine("Кiлькiсть обєктiв у Ghost2 (frequent entries cache): " + Ghost2.Count);

            Console.ForegroundColor = ConsoleColor.White;
        }

        public void Show() { ShowAllItems(); }
        #endregion


        #region Інкапсуляція методів
        private void AddItemInCache(Customer item) 
        {
            int CustomerCount = 0;
            int count = 0;
            bool historyMiss = !(Ghost1.Contains(item.IdCustomer) || Ghost2.Contains(item.IdCustomer));

            if(L1.Count + L2.Count == MaxSize)
                DelItemFromCache();  //Кеш заповнений повністю

            //Перевірка елементу на те що він часто додається в кеш
            count = ChechDuplicateItemInCache(item);

            if (count >= 3)
            {
                L2.Add(new ArcMember(item.IdCustomer, item) { TypeMember = TypeMember.L2, Count = 0 });
                //Предпологает то что удалит елемент с листа L1
                RemoveItemFromCache(item.IdCustomer);
            }

            if (historyMiss) //  || count >= 4 НЕ ПРАВИЛЬНОЕ УСЛОВИЕ. КОГДА ДОБАВЛЯЕТ В L2 (count >= 3) ДОБАВЛЯЕТ И СЮДА!!!!!!!!!!!
                L1.Add(new ArcMember(item.IdCustomer, item) { TypeMember = TypeMember.L1, Count = Interlocked.Increment(ref CustomerCount) });
            else // Елемент попадає в історію кешу
            {
                if (Ghost1.Contains(item.IdCustomer)) // Попадання в історію кешу B1
                    balance = Math.Min(balance + Math.Max(1, Ghost2.Count / Ghost2.Count), MaxSize);
                else // Попадання в історію кешу B2
                    balance = Math.Max(balance - Math.Max(1, Ghost1.Count / Ghost2.Count), 0);
            }
            _customerCurrent = item;
            
        }

        //count будет показивать сколько раз запись появлялаль в листе. 
        //Если такая записаь больше 3 раз то она есть частая и ми ее перезаписоваем в L2 а с L1 удаляем. Потому что она очень часто визиваеться. 
        private int ChechDuplicateItemInCache(Customer cust)
        {
            ArcMember item = null;
            int count = 0;
            SetToFind(cust.IdCustomer);

            for (int i = 0; i < L1.Count; i++)
            {
                item = L1.Find(GuidItem);
                if (item == null)
                    return 0;
                if(item.Customer == cust)
                    count++;
            }

            if (item != null)
                return count;
            else
                count = 0;

            return count;
        }

        //Знайти клієнта 
        private bool FindItem(Guid id, out Customer cust) 
        {
            //cust = null;
            // Встановлюємо ідентифікатор для пошуку обєкта
            SetToFind(id);      
            ArcMember item = L1.Find(GuidItem);

            if (item == null) 
                item = L2.Find(GuidItem);
            
            //Якщо знайшли то дадаємо в out Customer на вихід має клієнта
            cust = item.Customer;
            
            if(item == null && cust == null)
            {
                //Якщо досить не знайшли то йдемо шукати в списки "привиди" Ghosts 
                item.Customer.IdCustomer = Ghost1.Find(guid => guid == _guidToFind);

                if(item.Customer.IdCustomer == null)
                    // Якщо знайшли Guid до клієнта
                    item.Customer.IdCustomer = Ghost2.Find(_guid => _guid == _guidToFind); 
            
                // Робимо запит до БД на пошук такого клієнта за Guid та виводимо всю інформацію
                cust = Customer.GetCustomerById(item.Customer.IdCustomer); 
            }
 
            return (item == null && cust == null) ? false : true;
        }

        //Видалення елементу бо кеш повний
        private void DelItemFromCache() 
        {
            ArcMember item = null;
            bool found = false;
            do
            {
                if(L1.Count >= Math.Max(1, balance))
                {
                    found = true;
                    item = L1[L1.Count - 1]; // Знаходження останнього елементу
                    Ghost1.Add(item.GuId); // Додавання останнього елементу з L1 в Ghost1
                    L1.Remove(item);
                }
                else
                {
                    found = true;
                    item = L2[L2.Count - 1]; // Знаходження останнього елементу
                    Ghost2.Add(item.GuId); // Додавання останнього елементу з L2 в Ghost2
                    L2.Remove(item);
                }
                
            } while (!found);
        }

        //Видалення ПЕВНОГО клієнта
        private void RemoveItemFromCache(Guid itemId, bool addToGhost = true) 
        {
            SetToFind(itemId);
            ArcMember item = L1.Find(GuidItem);

            if(item != null)
            {
                if (item.TypeMember == TypeMember.L1 && addToGhost == true)
                    Ghost1.Add(item.GuId);
                //Тут надо продумать код для добавление в ПРИВИДЕНИЯ ЛИСТИ 
                for (int i = 0; i <= L1.Count; i++) // Тут проблема. НЕ УДАЛЯЕТ С КЕША ТЕ ЕЛЕМЕНТІ КОТОРІЕ СООТВЕТСВУЮТ ПОИСКУ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                {
                    L1.Remove(item);
                }
            }
            else
            {
                item = L2.Find(GuidItem);
                if(item !=null)
                {
                    if (item.TypeMember == TypeMember.L2 && addToGhost == true)
                        Ghost1.Add(item.GuId);
                    L2.Remove(item);
                }
                
            }
        }

        private void ShowAllItems()
        {
            Console.WriteLine("--------------------------Items in cache!----------------------");
            Console.WriteLine("All items in cache L1");
            foreach (var item in L1)
                Console.WriteLine(item.ToString());

            Console.WriteLine("All items in cache L2");
            foreach (var item in L2)
                if (L2.Count == 0)
                    Console.WriteLine("Cache is empty!");
                else
                    Console.WriteLine(item.ToString());

            Console.WriteLine("All items in cache Ghost1");
            foreach (var item in Ghost1)
                if (Ghost1.Count == 0)
                    Console.WriteLine("Cache is empty!");
                else
                    Console.WriteLine(item.ToString());

            Console.WriteLine("All items in cache Ghost2");
            foreach (var item in Ghost2)
                if (Ghost2.Count == 0)
                    Console.WriteLine("Cache is empty!");
                else
                    Console.WriteLine(item.ToString());
            
        }
        #endregion
    }
}

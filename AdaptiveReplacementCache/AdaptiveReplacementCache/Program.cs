﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdaptiveReplacementCache
{
    class Program
    {
        static readonly List<string> s = new List<string>();
        static void Main(string[] args)
        {
            //Console.ForegroundColor = ConsoleColor.Red;
            //int count = 0;
            //int C = Interlocked.Increment(ref count);
            //C = Interlocked.Increment(ref count);
            //C = Interlocked.Increment(ref count);
            //C = Interlocked.Increment(ref count);

            //Console.WriteLine(C);
            Customer Pasha = new Customer("Pasha", "Kiev", 18);
            Customer Diana = new Customer("Diana", "Kiev", 18);
            Customer Vlad = new Customer("Vlad", "Odessa", 19);
            Customer Dan = new Customer("Dan", "Kiev region", 18);
            Customer Swat = new Customer("Swat", "Polan", 18);


            MyCache<Customer> cache = new MyCache<Customer>();

            cache.AddItem(Pasha);
            cache.AddItem(Diana);
            cache.AddItem(Vlad);
            cache.ShowInformation();

            Console.Write("--------------------------Search customer by Guid!--------------");
            Customer find = cache.GetItem(Diana.IdCustomer);
            Console.WriteLine(find.ToString());

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("--------------------------Delete customer by Guid!--------------");
            Console.WriteLine("Delete is success!");
            cache.RemoveItem(Diana.IdCustomer);

            cache.ShowInformation();
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("--------------------------Add duplicart to cache!--------------");

            cache.AddItem(Dan);
            cache.AddItem(Dan);
            cache.AddItem(Dan);

            Console.ForegroundColor = ConsoleColor.White;
            cache.Show();

            Console.ForegroundColor = ConsoleColor.White;
            cache.ShowInformation();

            Console.ReadLine();
        }
    }
}
